require 'test_helper'

class UsersShowTest < ActionDispatch::IntegrationTest
  def setup
    @admin = users(:michael)
    @non_activated = users(:tanabe)
  end

  test "should now show non-activate user" do
    log_in_as(@admin)
    get user_path(@non_activated)
    assert_redirected_to root_url
  end

end
